activate_this = '/var/www/html/testapi/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
sys.path.insert(0, '/var/www/html/testapi')

from testapi.app import app as application