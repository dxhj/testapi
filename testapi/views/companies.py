# -*- coding: UTF-8 -*-
from flask import Blueprint, request
from sqlalchemy.exc import SQLAlchemyError
from testapi.database import db_session
from testapi.models import Company, Product
from testapi.utils.responses import success_response, error_response
from testapi.utils.validator import json_validator

blueprint = Blueprint('companies', __name__)


@blueprint.route('/companies', methods=['POST'])
def create_company():
    """ 
        Cria uma nova empresa

        método: POST
        entrada: name, cnpj, description, image (opcional), latitude, longitude
    """

    validator = json_validator('company', required=['name', 'cnpj', 'description', 'latitude', 'longitude'])
    if validator is not None:
        return validator

    data = request.get_json()
    name = data['name']
    cnpj = data['cnpj']
    description = data['description']
    image = data['image'] if 'image' in data else None
    latitude = data['latitude']
    longitude = data['longitude']

    if Company.query.filter_by(cnpj=cnpj).first() is not None:
        return error_response('O CNPJ informado já está cadastrado', 409)

    try:
        company = Company(name, cnpj, description, latitude, longitude, image)
        company.save()
    except SQLAlchemyError:
        return error_response('Não foi possível criar a empresa', 409)

    return success_response(company.as_dict(), 201)


@blueprint.route('/companies')
def show_companies():
    """ 
        Exibe todas as empresas

        método: GET
    """

    companies = Company.query.all()
    companies_response = {'companies': [company.as_dict() for company in companies]}
    return success_response(companies_response)


@blueprint.route('/companies/<int:id>', methods=['PATCH', 'PUT'])
def update_company(id):
    """ 
        Atualiza as informações da empresa

        métodos: PATCH, PUT
    """

    validator = json_validator('company')
    if validator is not None:
        return validator

    company = Company.query.get(id)
    if company is None:
        return error_response('A empresa não existe', 404)

    data = request.get_json()
    updated = False

    try:
        company.name = data['name']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        cnpj = data['cnpj']
        if Company.query.filter_by(cnpj=cnpj).first() is not None:
            return error_response('O CNPJ informado já está cadastrado', 409)
        company.cnpj = cnpj
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        company.description = data['description']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        company.image = data['image']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        company.latitude = data['latitude']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        company.longitude = data['longitude']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    if not updated:
        return error_response('Não há dados para atualizar', 422)

    return success_response(company.as_dict())


@blueprint.route('/companies/<int:id>', methods=['DELETE'])
def delete_company(id):
    """ 
        Apaga a empresa

        método: DELETE
    """

    company = Company.query.get(id)
    if company is None:
        return error_response('A empresa não existe', 404)

    db_session.delete(company)
    db_session.commit()

    return success_response(company.as_dict())


@blueprint.route('/companies/<int:id>')
def show_company(id):
    """
        Exibe a empresa por id

        método: GET
    """

    company = Company.query.get(id)
    if company is None:
        return error_response('A empresa não existe', 404)
    return success_response(company.as_dict())


@blueprint.route('/companies/<cnpj>/products', methods=['POST'])
def create_product(cnpj):
    """
        Cria um novo produto pertencente ao CNPJ da empresa

        método: POST
        entrada: name, description, image (opcional)
    """

    validator = json_validator('product', required=['name', 'description'])
    if validator is not None:
        return validator

    data = request.get_json()
    name = data['name']
    description = data['description']
    image = data['image'] if 'image' in data else None

    try:
        company = Company.query.filter_by(cnpj=cnpj).first()

        if company is None:
            return error_response('Nenhuma empresa cadastrada com o CNPJ especificado', 404)

        product = Product(name, description, company.cnpj, image)
        product.save()
    except SQLAlchemyError:
        return error_response('Não foi possível criar o produto', 409)

    return success_response(product.as_dict(), 201)


@blueprint.route('/companies/<cnpj>/products/<int:page>/<int:by>')
def show_company_products(cnpj, page, by):
    """
        Exibe os produtos pertencentes ao CNPJ especificado, utilizando paginação para os resultados, 
        página e itens por página

        método: GET
    """

    if Company.query.filter_by(cnpj=cnpj).first() is None:
        return error_response('Nenhuma empresa cadastrada com o CNPJ especificado', 404)

    page = 0 if page == 0 else page - 1
    products = Product.query.filter_by(company_cnpj=cnpj).limit(by).offset(page * by).all()
    products_response = {'products': [product.as_dict() for product in products]}
    return success_response(products_response)
