# -*- coding: UTF-8 -*-
from flask import Blueprint, request
from testapi.database import db_session
from testapi.models import Product
from testapi.utils.responses import success_response, error_response
from testapi.utils.validator import json_validator

blueprint = Blueprint('products', __name__)


@blueprint.route('/products/<int:id>', methods=['PATCH', 'PUT'])
def update_product(id):
    """ 
        Atualiza as informações do produto

        métodos: PATCH, PUT
    """

    validator = json_validator('product')
    if validator is not None:
        return validator

    product = Product.query.get(id)
    if product is None:
        return error_response('O produto não existe', 404)

    data = request.get_json()
    updated = False

    try:
        product.name = data['name']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        product.description = data['description']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        product.image = data['image']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    if not updated:
        return error_response('Não há dados para atualizar', 422)

    return success_response(product.as_dict())


@blueprint.route('/products/<int:id>', methods=['DELETE'])
def delete_product(id):
    """ 
        Apaga o produto

        método: DELETE
    """

    product = Product.query.get(id)
    if product is None:
        return error_response('O produto não existe', 404)

    db_session.delete(product)
    db_session.commit()

    return success_response(product.as_dict())


@blueprint.route('/products/<int:id>')
def show_product(id):
    """ 
        Exibe o produto por id

        método: GET
    """

    product = Product.query.get(id)
    if product is None:
        return error_response('O produto não existe', 404)
    return success_response(product.as_dict())


@blueprint.route('/products')
def show_products():
    """
        Exibe todos os produtos 

        método: GET
    """
    products = Product.query.all()
    products_response = {'products': [product.as_dict() for product in products]}
    return success_response(products_response)
