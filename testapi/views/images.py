# -*- coding: UTF-8 -*-
from flask import Blueprint, request
from testapi.utils.responses import success_response, error_response
from testapi.utils.validator import allowed_file
from testapi import config
from werkzeug.utils import secure_filename
from time import time
import boto3

blueprint = Blueprint('images', __name__)


@blueprint.route('/images/upload', methods=["POST"])
def image_upload():
    if 'image' not in request.files:
        return error_response('A requisição não contém uma imagem')

    image = request.files['image']
    if not image.filename:
        return error_response('Nenhuma imagem foi selecionada')

    if image and allowed_file(image.filename):
        filename = str(int(time())) + '.' + secure_filename(image.filename)[-3:]

        client = boto3.client('s3',
                              aws_access_key_id=config.AWS_ACCESS_KEY,
                              aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY)

        client.upload_fileobj(image, config.BUCKET_LOCATION, filename, ExtraArgs={
            'ACL': 'public-read',
            'ContentType': image.content_type})

        bucket_location = client.get_bucket_location(Bucket=config.BUCKET_LOCATION)

        image_response = {
             "image": "https://s3-{0}.amazonaws.com/{1}/{2}".format(bucket_location['LocationConstraint'],
                                                                    config.BUCKET_LOCATION,
                                                                    filename)
        }

        return success_response(image_response)

    return error_response('O tipo do arquivo não é permitido')
