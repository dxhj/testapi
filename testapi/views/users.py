# -*- coding: UTF-8 -*-
from flask import Blueprint, request
from sqlalchemy.exc import SQLAlchemyError
from testapi.database import db_session
from testapi.models import User
from testapi.utils.responses import success_response, error_response
from testapi.utils.validator import json_validator

blueprint = Blueprint('users', __name__)


@blueprint.route('/users', methods=['POST'])
def create_user():
    """ 
    Cria um novo usuário

    método: POST
    :entrada: name, email, latitude, longitude
    """

    validator = json_validator('user', required=['name', 'email', 'latitude', 'longitude'])

    if validator is not None:
        return validator

    data = request.get_json()

    name = data['name']
    email = data['email']
    latitude = data['latitude']
    longitude = data['longitude']

    if User.query.filter_by(email=email).first() is not None:
        return error_response('O email informado já está cadastrado', 409)

    try:
        user = User(name, email, latitude, longitude)
        user.save()
    except SQLAlchemyError:
        return error_response('Não foi possível criar o usuário', 409)

    return success_response(user.as_dict(), 201)


@blueprint.route('/users')
def show_users():
    """ 
        Exibe todos os usuários

        método: GET
    """

    users = User.query.all()
    users_response = {'users': [user.as_dict() for user in users]}
    return success_response(users_response)


@blueprint.route('/users/<int:id>', methods=['PATCH', 'PUT'])
def update_user(id):
    """ 
        Atualiza as informações do usuário

        métodos: PATCH, PUT
    """

    validator = json_validator('user')
    if validator is not None:
        return validator

    user = User.query.get(id)
    if user is None:
        return error_response('O usuário não existe', 404)

    data = request.get_json()
    updated = False

    try:
        user.name = data['name']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        email = data['email']
        if User.query.filter_by(email=email).first() is not None:
            return error_response('O email informado já está cadastrado', 409)
        user.email = email
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        user.latitude = data['latitude']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    try:
        user.longitude = data['longitude']
        db_session.commit()
        updated = True
    except KeyError:
        pass

    if not updated:
        return error_response('Não há dados para atualizar', 422)

    return success_response(user.as_dict())


@blueprint.route('/users/<int:id>', methods=['DELETE'])
def delete_user(id):
    """ 
        Apaga o usuário

        método: DELETE
    """

    user = User.query.get(id)
    if user is None:
        return error_response('O usuário não existe', 404)

    db_session.delete(user)
    db_session.commit()

    return success_response(user.as_dict())


@blueprint.route('/users/<int:id>')
def show_user(id):
    """ 
        Exibe o usuário por id

        método: GET
    """

    user = User.query.get(id)
    if user is None:
        return error_response('O usuário não existe', 404)
    return success_response(user.as_dict())


@blueprint.route('/users/<int:id>/companies/<int:page>/<int:by>')
def show_user_companies(id, page, by):
    """ 
        Seleciona as empresas próximas ao usuário (id) em um raio de 2 km, utilizando paginação para os resultados, 
        página e itens por página

        método: GET
    """

    user = User.query.get(id)
    if user is None:
        return error_response('O usuário não existe', 404)

    page = 0 if page == 0 else page - 1
    results = db_session.execute("SELECT *, ("
                                 "6371 * ACOS( "
                                 "COS( RADIANS( :lat ) ) "
                                 "* COS( RADIANS( latitude ) )"
                                 "* COS( RADIANS( longitude ) - RADIANS( :lng ) )"
                                 "+ SIN( RADIANS( :lat ) ) "
                                 "* SIN( RADIANS( latitude ) ) )) "
                                 "AS distance FROM companies HAVING distance < 2 ORDER BY distance"
                                 " LIMIT :by OFFSET :page",
                                 {'lat': user.latitude, 'lng': user.longitude, 'by': by, 'page': page * by})

    companies_response = {
        "companies": [
            {
                'id': company[0],
                'name': company[1],
                'cnpj': company[2],
                'description': company[3],
                'image': company[4],
                'latitude': company[5],
                'longitude': company[6],
            } for company in sorted(results.fetchall(), key=lambda y: y[0])
        ]
    }

    return success_response(companies_response)