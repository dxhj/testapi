# -*- coding: UTF-8 -*-
from flask import Flask
from database import db_session
from utils.responses import error_response
from views.users import blueprint as users
from views.companies import blueprint as companies
from views.products import blueprint as products
from views.images import blueprint as images
from views.frontend import blueprint as frontend

app = Flask(__name__)
app.register_blueprint(users, url_prefix='/api')
app.register_blueprint(companies, url_prefix='/api')
app.register_blueprint(products, url_prefix='/api')
app.register_blueprint(images, url_prefix='/api')
app.register_blueprint(frontend)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.errorhandler(404)
def page_not_found(e):
    return error_response('Recurso não encontrado', 404)


@app.errorhandler(405)
def method_not_allowed(e):
    return error_response('Método não permitido', 405)


@app.errorhandler(500)
def internal_server_error(e):
    return error_response('Erro interno', 500)
