# -*- coding: UTF-8 -*-
from flask import request
from jsonschema import validate, ValidationError
from responses import error_response

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def json_validator(name, required=[]):
    predefined_rules = {
        'user': {
            'name': {
                'type': 'string',
                'minLength': 6,
                'maxLength': 255,
            },
            'email': {
                'type': 'string',
                'minLength': 3,
                'pattern': '^\S+@\S+$',
            },
            'latitude': {
                'type': 'number'
            },
            'longitude': {
                'type': 'number'
            },
        },
        'company': {
            'name': {
                'type': 'string',
                'minLength': 3,
                'maxLength': 255,
            },
            'cnpj': {
                'type': 'string',
                'minLength': 14,
                'maxLength': 14,
                'pattern': '^\d+$',
            },
            'description': {
                'type': 'string',
                'minLength': 6,
                'maxLength': 255,
            },
            'image': {
                'type': 'string',
                'minLength': 5,
                'maxLength': 255,
                'pattern': '^.+\.(png|jpg|jpeg|gif)$',
            },
            'latitude': {
                'type': 'number'
            },
            'longitude': {
                'type': 'number'
            },
        },
        'product': {
            'name': {
                'type': 'string',
                'minLength': 3,
                'maxLength': 255,
            },
            'description': {
                'type': 'string',
                'minLength': 6,
                'maxLength': 255,
            },
            'image': {
                'type': 'string',
                'minLength': 5,
                'maxLength': 255,
                'pattern': '^.+\.(png|jpg|jpeg|gif)$'
            },
        }
    }

    if not required:
        schema = {
            'type': 'object',
            'properties': predefined_rules[name],
        }
    else:
        schema = {
            'type': 'object',
            'properties': predefined_rules[name],
            'required': required
        }

    if not request.is_json:
        return error_response('Os dados de entrada devem estar no formato JSON')

    try:
        data = request.get_json()
    except:
        return error_response('Não foi possível ler os dados JSON')

    try:
        validate(data, schema)
    except ValidationError:
        return error_response('Os dados são inválidos')


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
