from flask import current_app
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import config


engine = create_engine('mysql+mysqldb://{0}:{1}@{2}/{3}?charset=utf8'.format(config.DB_USER,
                                                                             config.DB_PASS,
                                                                             config.DB_HOST,
                                                                             config.DB_DATABASE),
                       convert_unicode=True)

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    import models
    Base.metadata.create_all(bind=engine)
