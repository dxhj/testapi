from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import relationship
from database import Base, db_session


class User(Base):
    __tablename__ = 'users'
    __table_args__ = {
        'mysql_charset': 'utf8',
        'mysql_collate': 'utf8_unicode_ci'
    }

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False, unique=True)
    latitude = Column(Float(precision=6), nullable=False)
    longitude = Column(Float(precision=6), nullable=False)

    def __init__(self, name, email, latitude, longitude):
        self.name = name
        self.email = email
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return '<User %r>' % self.name

    def save(self):
        db_session.add(self)
        db_session.commit()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Company(Base):
    __tablename__ = 'companies'
    __table_args__ = {
        'mysql_charset': 'utf8',
        'mysql_collate': 'utf8_unicode_ci'
    }

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    cnpj = Column(String(15), nullable=False, unique=True)
    description = Column(String(255), nullable=False)
    image = Column(String(255))
    latitude = Column(Float(precision=6), nullable=False)  # Latitude
    longitude = Column(Float(precision=6), nullable=False)  # Longitude
    products = relationship('Product', cascade="all,delete")

    def __init__(self, name, cnpj, description, latitude, longitude, image=None):
        self.name = name
        self.cnpj = cnpj
        self.description = description
        self.image = image
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return '<Company %r>' % self.name

    def save(self):
        db_session.add(self)
        db_session.commit()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Product(Base):
    __tablename__ = 'products'
    __table_args__ = {
        'mysql_charset': 'utf8',
        'mysql_collate': 'utf8_unicode_ci'
    }

    id = Column(Integer, primary_key=True)
    company_cnpj = Column(String(60), ForeignKey('companies.cnpj'), nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(String(255), nullable=False)
    image = Column(String(255))

    def __init__(self, name, description, company_cnpj, image=None):
        self.name = name
        self.description = description
        self.company_cnpj = company_cnpj
        self.image = image

    def __repr__(self):
        return '<Product %r>' % self.name

    def save(self):
        db_session.add(self)
        db_session.commit()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}